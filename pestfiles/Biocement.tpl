ptf ~
[Newton]
MaxRelativeShift =1e-7 #5e-8
MaxSteps = 6 #
TargetSteps = 4 #

[Problem]
Name = TR1-2kPowderMax.NoBio
RegulCalciteDetach = 0.5

[Initial]
initDensityW = 1087 # 			[kg/m³]
initPressure = 1e5 # 			[Pa]

initxwTC = 2.3864e-7 #	 		[mol/mol]
initxwNa = 0.0 # 				[mol/mol]
initxwCl = 0.0 # 				[mol/mol]
initxwCa = 0.0 # 				[mol/mol]
initxwUrea = 0.0 # 				[mol/mol]
initxwTNH = 3.341641e-3 #	 	[mol/mol]

initCalcite = 0.01718131868  #  [-]
initCalciteInSand = 0.009027472527  #  [-]
xwNaCorr = 3.422270053e-07 #    [mol/mol]
PercentPowder = ~P~           # [-] TR1-2 has 1% powder
initTemperature = 298.15 #      [K] 25°C

xwClCorr = 0.0 # 				[mol/mol]

[Injection]
injDensity = 1067    #          [kg/m³] TR1-2 has 0.75M mineralisation solution, density for which is according to ILEK ####Update, when using different concentration!

injTC = 1.044e-06 #             [kg/kg]
injNa = 0          #            [kg/m³]
injCa = 30         #            [kg/m³]
injUrea = 45.045     #          [kg/m³]
injTNH = 0          #           [kg/m³]
injNaCorr = 0.0001626083724 #   [kg/m³] TODO: check!!

numInjections = 53 #
RateFile = injections/injection_tr1-2.dat
CheckpointsFile = injections/injection_checkpoints_tr1-2.dat

[TimeLoop]
DtInitial = 1 #0.01# [s]
TEnd = 404040 # [s]		# see Durchflussrate_InjectionStrategy
MaxTimeStepSize = 200 # [s]

[Grid]
UpperRight = 0.2  # [m] upper right corner coordinates, experiment has 15 cm, let us extend the domain by 5 cm to minimize boundary effects
Cells = 40  # [-] number of cells in x,y-direction

[SpatialParams]
ReferencePorosity = 0.31 # [-]
CritPorosity = 0.0 # [-]
AverageGrainDiameter = 0.0005 # [-]
Sphericity = 1.9  # [-]
ReferencePermeability = 1.28e-12 # reference permeability calculated using Kozeny-Carman # ReferencePermeability = 2e-10 # [m^2]
                                 # ref perm calculated from 11,40 ml/min in the first injection interval yields 1,28e-12 [m^2]
Exponent = ~E~
BrooksCoreyPcEntry = 1000 # [Pa]
BrooksCoreyLambda = 2 #

#[BioCoefficients]
#dc0 = 3.183e-7 # 				// [1/s] 		Taylor and Jaffe 1990
#rhoBiofilm = 6.9 #10 # 			//[kg/m³] 		kurease fit 2014_10_2, fittet parameter!!!!!!!!!!!!

[CalciteCoefficients]
ac = 2000 # 		// [1/dm] 		Ebigbo et al. 2012  (estimated)
kdiss1 = 8.9e-3 # 	// [kgH2O/dm²s] Chou et al. 1989
kdiss2 = 6.5e-9 #  	// [mol/dm²s] 	Chou et al. 1989
kprec = 1.5e-12 # 	// [mol/dm²s] 	Zhong and Mucci 1989
ndiss = 1.0 #0.045625 		// [-] 			Flukinger and Bernard 2009
nprec = 3.27 # 		// [-] 			Zhong and Mucci 1989
Asw0 = ~Asw~	#// 500 [1/dm] 		Ebigbo et al. 2012  (estimated using phi_0 and A/V)
MaxDCalc = 20.0 #       // [mikrometer]
UreaseDiffLimitCoeff = 0.5
UreaseDiffLimitResidualPercentage = 0.006
[UreolysisCoefficients]
kub = 0.01 #             // [kg_urease/kg_bio]       calculated from kurease fit 2014_10_2, fittet parameter!!!!!!!!!!!!
kPowderMax = 0.09125 # Der Wert ist unverständlicherweise doppelt so groß als bei Zhijun, keine Ahnung warum er 0.045625 hatte (Faktor 2)
cureaseT = -4263.108 # [K] or rather  exponent for the temperature dependence (Oct. 2019, see KineticsCalculation spreadsheet)
KureaseModell = 706.6667 #

[Neumann]
faktor = 0.95
refInjectionQ = 2e-7
