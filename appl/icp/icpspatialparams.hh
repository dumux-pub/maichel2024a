// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_ICP_SPATIAL_PARAMS_HH
#define DUMUX_ICP_SPATIAL_PARAMS_HH

#include <dumux/porousmediumflow/fvspatialparamsmp.hh>
#include <dumux/material/fluidmatrixinteractions/2p/brookscorey.hh>
#include <dumux/material/fluidmatrixinteractions/porosityprecipitation.hh>
#include <dumux/material/fluidmatrixinteractions/permeabilityvermapruess.hh>


namespace Dumux{
//forward declaration
template<class TypeTag>
class ICPSpatialParams;

/*!
* \ingroup 2PICPModel
* \brief Definition of the spatial parameters for the two-phase
* induced calcium carbonate precipitation problem.
*/
// template<class TypeTag>
// class ICPSpatialParams : public FVSpatialParams<TypeTag>
template<class TypeTag>
class ICPSpatialParams
: public FVSpatialParams<GetPropType<TypeTag, Properties::GridGeometry>,
                         GetPropType<TypeTag, Properties::Scalar>,
                         ICPSpatialParams<TypeTag>>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using ParentType = FVSpatialParams<GridGeometry, Scalar, ICPSpatialParams<TypeTag>>;
    using PcKrSwCurve = FluidMatrix::BrooksCoreyDefault<Scalar>;
    using GridView = typename GridGeometry::GridView;
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using CoordScalar = typename GridView::ctype;
    enum { dimWorld=GridView::dimensionworld };

    using GlobalPosition = Dune::FieldVector<CoordScalar, dimWorld>;
    using Tensor = Dune::FieldMatrix<CoordScalar, dimWorld, dimWorld>;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using Element = typename GridView::template Codim<0>::Entity;

public:
    using SolidSystem = GetPropType<TypeTag, Properties::SolidSystem>;
    // type used for the permeability (i.e. tensor or scalar)
    using PermeabilityType = Scalar;

    ICPSpatialParams(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    , pcKrSwCurve_("SpatialParams")
    {
        try
        {
        referencePorosity_     = getParam<Scalar>("SpatialParams.ReferencePorosity", 0.4);
        referencePermeability_ = getParam<Scalar>("SpatialParams.ReferencePermeability", 2.e-10);
        critPorosity_ = getParam<Scalar>("SpatialParams.CritPorosity", 0.0);
	exponent_ = getParam<Scalar>("SpatialParams.Exponent", 1000);
        }
        catch (Dumux::ParameterException &e)
        {
            std::cerr << e << ". Abort!\n";
            exit(1) ;
        }
    }

    /*!
     *  \brief Define the reference porosity \f$[-]\f$ distribution.
     *  This is the porosity of the porous medium without any of the
     *  considered solid phases.
     *
     *  \param element The finite element
     *  \param scv The sub-control volume
     */
    Scalar referencePorosity(const Element& element, const SubControlVolume &scv) const
    { return referencePorosity_; }

    /*!
     *  \brief Define the volume fraction of the inert component
     *
     *  \param element The finite element
     *  \param scv The sub-control volume
     *  \param compIdx The index of the inert solid component
     */
    template<class SolidSystem, class ElementSolution>
    Scalar inertVolumeFraction(const Element& element,
                    const SubControlVolume& scv,
                    const ElementSolution& elemSol,
                    int compIdx) const
    { return 1.0-referencePorosity_; }

    /*!
     *  \brief Define the volume fraction of the inert component
     *
     *  \param globalPos The global position in the domain
     *  \param compIdx The index of the inert solid component
     */
    template<class SolidSystem>
    Scalar inertVolumeFractionAtPos(const GlobalPosition& globalPos, int compIdx) const
    { return 1.0-referencePorosity_; }

    /*!
     *  \brief Return the actual recent porosity \f$[-]\f$ accounting for
     *  clogging caused by mineralization
     *
     *  \param element The finite element
     *  \param scv The sub-control volume
     */
    template<class ElementSolution>
    Scalar porosity(const Element& element,
                    const SubControlVolume& scv,
                    const ElementSolution& elemSol) const
    { 
	    const auto& globalPos = scv.dofPosition();
	    auto poro = poroLaw_.evaluatePorosity(element, scv, elemSol, referencePorosity_);
	    return poro; 
    }


    /*! Intrinsic permeability tensor K \f$[m^2]\f$ depending
     *  on the position in the domain
     *
     *  \param element The finite volume element
     *  \param scv The sub-control volume
     *
     *  Solution dependent permeability function
     */
    template<class ElementSolution>
    PermeabilityType permeability(const Element& element,
                        const SubControlVolume& scv,
                        const ElementSolution& elemSol) const
    {
	const auto& globalPos = scv.dofPosition();
        const auto poro = porosity(element, scv, elemSol);
	//std::cout << "jetzt in oberer permeability Funktion von icpspatialparams" ;
	auto perm = permLaw_.evaluatePermeability(referencePermeability_, referencePorosity_, critPorosity_, poro, exponent_);
	if(globalPos[0] > 0.135)
		perm = perm*20;
        return perm;
    }

   /*! Intrinsic permeability tensor K \f$[m^2]\f$ depending
     *  on the position in the domain
     *
     *  \param element The finite volume element
     *  \param scv The sub-control volume
     *
     *  Solution dependent permeability function
     */
    template<class ElementSolution>
    PermeabilityType permeability(const Element& element,
                        const SubControlVolume& scv,
                        const ElementSolution& elemSol,
                        const Scalar porosity) const
    {

	//std::cout << "bin jetzt in der unteren Perm-Funktion" ;
        static const auto expo = getParam<Scalar>("SpatialParams.Exponent");
        return permLaw_.evaluatePermeability(referencePermeability_, referencePorosity_, critPorosity_, porosity, expo);
    }

    /*!
     * \brief Returns the fluid-matrix interaction law at a given location
     *
     *  \param element The finite volume element
     *  \param scv The sub-control volume
     *
     */
    template<class ElementSolution>
    auto fluidMatrixInteraction(const Element& element,
                        const SubControlVolume& scv,
                        const ElementSolution& elemSol) const
    {
        const auto globalPos = scv.center();
        return fluidMatrixInteractionAtPos(globalPos);
    }

    /*!
     * \brief Returns the fluid-matrix interaction law at a given location
     * \param globalPos A global coordinate vector
     */
    auto fluidMatrixInteractionAtPos(const GlobalPosition &globalPos) const
    {
        return makeFluidMatrixInteraction(pcKrSwCurve_);
    }

    /*!
     * \brief Function for defining which phase is to be considered as the wetting phase.
     *
     * \return the wetting phase index
     * \param globalPos The position of the center of the element
     */
    template<class FluidSystem, class ElementSolution>
    int wettingPhase(const Element& element,
                        const SubControlVolume& scv,
                        const ElementSolution& elemSol) const
    { return FluidSystem::H2OIdx; }

    /*!
     * \brief Function for defining which phase is to be considered as the wetting phase.
     *
     * \return the wetting phase index
     * \param globalPos The position of the center of the element
     */
    template<class FluidSystem>
    int wettingPhaseAtPos(const GlobalPosition& globalPos) const
    { return FluidSystem::H2OIdx; }

private:

//     MaterialLawParams materialParams_;

    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
    using PorosityLaw = PorosityPrecipitation<Scalar, ModelTraits::numFluidComponents(), SolidSystem::numComponents - SolidSystem::numInertComponents>;
    PorosityLaw poroLaw_;
    PermeabilityVermaPruess<PermeabilityType> permLaw_;
    Scalar referencePorosity_;
    Scalar critPorosity_;
    Scalar exponent_;
    PermeabilityType referencePermeability_ = 0.0;

    const PcKrSwCurve pcKrSwCurve_;
};

} // end namespace Dumux

#endif
