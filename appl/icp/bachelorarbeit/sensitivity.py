import pyvista
import numpy as np
import json 
import os
import pandas as pd
from run_tests import generate_tasks
def get_uniformity_porosity(pvd_file:str) -> float:
    # read in pvd file and set state to the end of the simulation
    reader = pyvista.get_reader(pvd_file)
    reader.set_active_time_value(reader.time_values[-1])
    data = reader.read()[0]

    # extract the points between 0 and 0.135
    data.point_data['x_coord'] = data.points[:, 0]
    extracted_data = data.threshold(value=(0,0.135), scalars='x_coord', invert=False)

    p1_id = int(len(extracted_data.points) / 3) # 1/3 from the bottom
    p2_id = int(len(extracted_data.points) * 2 / 3) # 2/3 from the bottom
    uniformity_porosity = extracted_data.point_data['porosity'][p2_id] / extracted_data.point_data['porosity'][p1_id]
    return uniformity_porosity

def get_average_porosity(input_file:str) -> float:
    with open(input_file, 'r') as f:
        lines = f.readlines()
    #we directly read the last entry in dat file
    return float(lines[-1].split()[1])

def selected_compared_points(num_compared_points:int = 5, printed:bool = False):
    # read in the ref file and drop all zero values
    with open ("../injections/refinjectionrates.dat","r") as f:
        lines = f.readlines()
        ref_injection_rate = {}
        for line in lines[1:]:
            if (float(line.split()[1]) == 0.0):
                continue
            ref_injection_rate[float(line.split()[0])] = float(line.split()[1])

    # read in check points and drop the 0 values
    check_points = np.loadtxt("../injections/injection_checkpoints_tr1-2.dat")
    filtered_check_points = [ t for t in check_points if t in ref_injection_rate ]
    
    # calculate select points
    # e.g. 1/6, 2/6, ...5/6 
    points = [ filtered_check_points[-1] * (i+1)  / (num_compared_points + 1) for i in range(0,num_compared_points)]
    closed_points_in_ref = []
    for time in points:
        ref_time = np.array(list(ref_injection_rate.keys()))
        idx = np.where(abs(ref_time - time) == np.min(abs(ref_time - time)))
        closed_points_in_ref.append(ref_time[idx][0])

    ref_values = np.array([ref_injection_rate[t] for t in closed_points_in_ref])

    if printed:
        print(f"reference time {closed_points_in_ref}")
        print(f"with injection rates {ref_values}")
    return(closed_points_in_ref, ref_values)

def get_injection_rate_deviation(input_file:str, ref_time, ref_rate, verbal = False) -> float:
    #read in injection rates
    with open (input_file,"r") as f:
        lines = f.readlines()
        injection_rate = {}
        for line in lines[1:]:
            if (float(line.split()[1]) == 0.0):
                continue
            injection_rate[float(line.split()[0])] = float(line.split()[1])

    #find the closet time to ref_time
    sim_time = np.array(list(injection_rate.keys()))
    selected_time = []
    for time in ref_time:
        idx = np.where(abs(sim_time - time) == np.min(abs(sim_time - time)))[0][0]
        selected_time.append(sim_time[idx])
 
    selected_rate=np.array([injection_rate[t]for t in selected_time])
    if np.any(selected_rate==0.0):
        print("Attention: 0 injection rate is selected!")
    delta = selected_rate - ref_rate
    deviation = np.linalg.norm(delta)
    if verbal:
        print(f"case {os.getcwd().split('/')[-1]}")
        print(f"compared time {selected_time}")
        print(f"compared rate {selected_rate}")
        print(f"deviation: {deviation}")
        print("\n\n")
    return deviation

if __name__ == "__main__":
    verbal = True
    num_compared_points = 5
    with open("sensitivity_input.dict", "r") as f:
        params = json.load(f)
    tasks = generate_tasks(params)
    results = {}
    for task in tasks:
        os.chdir(task)
        avg_phi = get_average_porosity("avgporoovertime.dat")
        uniformity = get_uniformity_porosity("TR1-2kPowderMax.NoBio.pvd")
        ref_time,ref_rate = selected_compared_points(num_compared_points, verbal)
        deviation = get_injection_rate_deviation("injectionovertime.dat",ref_time,ref_rate, verbal)
        results[task] = {"Group": task.split("_")[0],
                         "avg_phi": avg_phi,
                         "uniformity": uniformity,
                         "deviation": deviation}
        os.chdir("..")
    pd.set_option('display.float_format', '{:.6g}'.format)
    results = pd.DataFrame(results)
    
    csv_file_path = 'sensitivity.csv'
    results.to_csv(csv_file_path, index=False)
    results.to_pickle("sensitivity.pkl")

    print(results)
    
