import multiprocessing
import subprocess
import json
import os
def generate_tasks(params:dict) -> list:
    cases = list(params.keys())
    cases_list = [ f"{c}_{i}" for i in ["plus","minus"] for c in cases]
    cases_list.append("ref")
    print(f"generate case lists: {cases_list}")
    return cases_list
    
def run_tasks_on_single_cpu(path_list:list, test_name = "biocement_column_test"):
    for path in path_list:
        os.chdir(path)
        with open("run.log", "a") as log:
            with subprocess.Popen(
                [f"./{test_name}", f"{path}.input"],
                stdout=log,
                stderr=log,
                universal_newlines=True,
                cwd=".",
            ) as popen:
                returnCode = popen.wait()
            if returnCode != 0:
                print(f"Error: test case {path} returned f{returnCode}!")
            if returnCode == 0:
                print(f"test case {path} done.")
        os.chdir("..")
    
if __name__ == "__main__":
    with open("sensitivity_input.dict", "r") as f:
        params = json.load(f)
    tasks = generate_tasks(params)
    
    num_processors = multiprocessing.cpu_count()
    print(f"use {num_processors} processors, adjusting num_processors if necessary")
    
    if num_processors > len(tasks):
        chunks = [tasks[i:i+1] for i in range(len(tasks))]
        
    else:
        # Divide tasks among processors
        chunk_size = len(tasks) // num_processors
        chunks = [tasks[i:i + chunk_size] for i in range(0, len(tasks), chunk_size)]
        if len(chunks) > num_processors:
            chunks[-2].extend(chunks[-1])
            chunks = chunks[0:-1]
            
    processes = []
    
    for chunk in chunks:
        p = multiprocessing.Process(target=run_tasks_on_single_cpu, args=(chunk,))
        processes.append(p)
        p.start()

    # Wait for all processes to finish
    for p in processes:
        p.join()

    print("All tasks are done!")
