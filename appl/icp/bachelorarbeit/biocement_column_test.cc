// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Test for the the fully implicit two-phase enzymatically induced calcium carbonate
 * precipitation model.
 */
#include <config.h>

#include <ctime>
#include <iostream>
#include <dumux/io/container.hh>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/grid/io/file/dgfparser/dgfexception.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/istl/io.hh>

#include "properties.hh"

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/defaultusagemessage.hh>

#include <dumux/linear/amgbackend.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include <dumux/assembly/fvassembler.hh>
#include <dumux/assembly/diffmethod.hh>

#include <dumux/discretization/method.hh>

#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/grid/gridmanager.hh>
#include <dumux/io/loadsolution.hh>


/*!
 * \brief Provides an interface for customizing error messages associated with
 *        reading in parameters.
 *
 * \param progName  The name of the program, that was tried to be started.
 * \param errorMsg  The error message that was issued by the start function.
 *                  Comprises the thing that went wrong and a general help message.
 */
void usage(const char *progName, const std::string &errorMsg)
{
    if (errorMsg.size() > 0) {
        std::string errorMessageOut = "\nUsage: ";
                    errorMessageOut += progName;
                    errorMessageOut += " [options]\n";
                    errorMessageOut += errorMsg;
                    errorMessageOut += "\n\nThe list of mandatory options for this program is:\n"
                                        "\t-ParameterFile Parameter file (Input file) \n";

        std::cout << errorMessageOut
                  << "\n";
    }
}

int main(int argc, char** argv) try
{
    using namespace Dumux;

    // define the type tag for this problem
    using TypeTag = Properties::TTag::TYPETAG;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv, usage);

    // try to create a grid (from the given grid file or the input file)
    GridManager<GetPropType<TypeTag, Properties::Grid>> gridManager;
    gridManager.init();

    ////////////////////////////////////////////////////////////
    // run instationary non-linear problem on this grid
    ////////////////////////////////////////////////////////////

    // we compute on the leaf grid view
    const auto& leafGridView = gridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    auto gridGeometry = std::make_shared<GridGeometry>(leafGridView);

    // the problem (initial and boundary conditions)
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    auto problem = std::make_shared<Problem>(gridGeometry);

    // get some time loop parameters
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    const auto tEnd = getParam<Scalar>("TimeLoop.TEnd");
    const auto maxDt = getParam<Scalar>("TimeLoop.MaxTimeStepSize");
    auto dt = getParam<Scalar>("TimeLoop.DtInitial");

    // check if we are about to restart a previously interrupted simulation
    Scalar restartTime = getParam<Scalar>("Restart.Time", 0);

    // the solution vector
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    SolutionVector x(gridGeometry->numDofs());
    if (restartTime > 0)
    {
        using IOFields = GetPropType<TypeTag, Properties::IOFields>;
        using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
        using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
        using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
        using SolidSystem = GetPropType<TypeTag, Properties::SolidSystem>;
        const auto fileName = getParam<std::string>("Restart.File");
        const auto pvName = createPVNameFunction<IOFields, PrimaryVariables, ModelTraits, FluidSystem, SolidSystem>();
        loadSolution(x, fileName, pvName, *gridGeometry);
    }
    else
        problem->applyInitialSolution(x);
    auto xOld = x;

    // the grid variables
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    auto gridVariables = std::make_shared<GridVariables>(problem, gridGeometry);
    gridVariables->init(x);

    // initialize the vtk output module
    using IOFields = GetPropType<TypeTag, Properties::IOFields>;
    VtkOutputModule<GridVariables, SolutionVector> vtkWriter(*gridVariables, x, problem->name());
    using VelocityOutput = GetPropType<TypeTag, Properties::VelocityOutput>;
    vtkWriter.addVelocityOutput(std::make_shared<VelocityOutput>(*gridVariables));
    IOFields::initOutputModule(vtkWriter); //!< Add model specific output fields
    //add specific output
    vtkWriter.addField(problem->getPermeability(), "Permeability");
    // update the output fields before write
    problem->updateVtkOutput(x);
    vtkWriter.write(restartTime);

    // instantiate time loop
    int episodeIdx_ = 0;
    // set the initial episodeIdx in the problem to zero
    problem->setEpisodeIdx(episodeIdx_);
    // set the time loop with episodes (check points)
    auto timeLoop = std::make_shared<CheckPointTimeLoop<Scalar>>(restartTime, dt, tEnd);
    timeLoop->setMaxTimeStepSize(maxDt);

    // do it with episodes if an injection parameter file is specified
//     std::vector<Scalar> injType_;
    if (hasParam("Injection.CheckpointsFile"))
    {
        //We first read the times of the checkpoints from the injection file
        const auto injectionCheckPoints = readFileToContainer<std::vector<double>>(getParam<std::string>("Injection.CheckpointsFile"));

        // We set the time loop with episodes of various lengths as specified in the injection file
        // set the episode ends /check points:
        timeLoop->setCheckPoint(injectionCheckPoints);

        // set the initial episodeIdx in the problem to zero
        problem->setEpisodeIdx(episodeIdx_);
    }

    // fixed episode lengths for convenience in output only if this is specified
    else if (hasParam("TimeLoop.EpisodeLength"))
    {
        // set the time loop with periodic episodes of constant length
        timeLoop->setPeriodicCheckPoint(getParam<Scalar>("TimeLoop.EpisodeLength"));
    }

    // no episodes
    else
    {
        // set the time loop with one big episodes
        timeLoop->setCheckPoint(tEnd);
    }

    // the assembler with time loop for instationary problem
    using Assembler = FVAssembler<TypeTag, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(problem, gridGeometry, gridVariables, timeLoop, xOld);

    // the linear solver
    using LinearSolver = Dumux::ILU0BiCGSTABBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = NewtonSolver<Assembler, LinearSolver>;
    NewtonSolver nonLinearSolver(assembler, linearSolver);
	
    //output injectionrate
    double currentInjectionRate=0, currentAveragePoro=0;
    std::ofstream outputfileinj, outputfileavgporo;
    outputfileinj.open("injectionovertime.dat",std::ios::out);
    outputfileinj << std::scientific << std::setprecision(4);
    outputfileinj << "#Time[s]" << "\t" << "injectionrate [Kg/s]" << std::endl;
    outputfileavgporo.open("avgporoovertime.dat",std::ios::out);
    outputfileavgporo << std::scientific << std::setprecision(4);
    outputfileavgporo << "#Time[s]" << "\t" << "average porosity [-]" << std::endl;
    // time loop
    
    timeLoop->start(); do
    {
        // set time for problem for implicit Euler scheme
        problem->setTime( timeLoop->time() + timeLoop->timeStepSize() );
        problem->setTimeStepSize( timeLoop->timeStepSize() );
	
	//abort simulation if timeStepSize is smaler than 10^-9 (PEST-compatibility)
	double currentStepSize = timeLoop->timeStepSize();
	 if (currentStepSize < 1e-5) {
            std::cout << "Time step size is too small. Aborting simulation." << std::endl;
            break;
	}
	// set flag for updating the injection rate at the boundary
	// This is very very problem specific for this application
	problem->setFlag(0);

        // set previous solution for storage evaluations
        assembler->setPreviousSolution(xOld);

        // solve the non-linear system with time step control
        nonLinearSolver.solve(x, *timeLoop);

        // make the new solution the old solution
        xOld = x;
        gridVariables->advanceTimeStep();

        // advance to the time loop to the next step
        timeLoop->advanceTimeStep();

        //only write output at checkpoints or every 500th time step
        if (timeLoop->timeStepIndex() % 500 == 0
                || timeLoop->isCheckPoint())
        {
            // update the output fields before write
            problem->updateVtkOutput(x);

            // write vtk output
            vtkWriter.write(timeLoop->time());
        }

	// Get postprocessing information
	const auto averagePoro = problem->getAveragePoro(*gridVariables, x);
	currentAveragePoro= averagePoro;
	outputfileavgporo << timeLoop->time() << "\t" << currentAveragePoro << std::endl;

        // report statistics of this time step
        timeLoop->reportTimeStep();

        // report and count episodes and tell the problem when advancing to the next episode
        if (hasParam("Injection.CheckpointsFile") || hasParam("TimeLoop.EpisodeLength"))
        {
            if(timeLoop->isCheckPoint() && hasParam("Injection.CheckpointsFile"))
            {
                episodeIdx_++;
                problem->setEpisodeIdx(episodeIdx_);
                std::cout << "\n Episode " << episodeIdx_ << " done. Starting next."<< std::endl;

	        currentInjectionRate= problem->getInjectionRate();
	        outputfileinj << timeLoop->time() << "\t" << currentInjectionRate << std::endl;
            }
        }

        // set new dt as suggested by the newton solver
        timeLoop->setTimeStepSize(nonLinearSolver.suggestTimeStepSize(timeLoop->timeStepSize()));

	if(timeLoop->timeStepIndex() == 1){
	currentInjectionRate= problem->getInjectionRate();
	outputfileinj << timeLoop->time() << "\t" << currentInjectionRate << std::endl;
	}

    } while (!timeLoop->finished());
    problem->writeFinalPorosities(x);
    timeLoop->finalize(leafGridView.comm());

	outputfileinj.close();
	outputfileavgporo.close();

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}

