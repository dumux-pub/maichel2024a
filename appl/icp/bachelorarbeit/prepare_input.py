import os
import shutil
import json

#generate input file for each case
def prepare_input(input_file:str, params:dict, delta = 0.1, test_name = "biocement_column_test"):
    """_summary_

    Args:
        input_file (str): input reference file
        params (dict): varying parameters
        delta (float, optional): change to reference value Defaults to 0.1.
        test_name (str): the name of binary file
    Raises:
        ValueError: if parameters are not detected in input file
    """
    #remove previous generated inputs
    current_directory = os.getcwd()
    files = os.listdir(current_directory)
    extension_to_remove = '.input'

    for file_name in files:
        if file_name.endswith(extension_to_remove):
            # we need to keep the original input file
            if file_name == input_file:
                continue
            file_path = os.path.join(current_directory, file_name)
            os.remove(file_path)
            print(f"Removed: {file_path}")

    #remove previous generated folders like A_plus/minus
    for folder_name in os.listdir(current_directory):
        folder_path = os.path.join(current_directory, folder_name)

        for name in ["plus","minus","ref"]:
            # Check if the folder name contains the word "plus"
            if name in folder_name and os.path.isdir(folder_path):
                try:
                    # Remove the directory and its contents
                    shutil.rmtree(folder_path)
                    print(f"Removed folder: {folder_path}")
                except Exception as e:
                    print(f"Error while removing folder {folder_path}: {e}")

    # generate new inputs and move them into the respective folders
    with open(input_file, "r") as f:
        lines = f.readlines()

    for key, param_name in params.items():
        key_found = False
        line_idx = 0
        for idx,line in enumerate(lines):
            if param_name in line:
                line_idx = idx
                key_found = True

        if not key_found:
            raise ValueError(f"Param {param_name} not found in {inputFile}")

        orig_value = float(lines[line_idx].split("#")[0].split("=")[1].strip())
        plus_value = orig_value * (1.0 + delta)
        minus_value = orig_value * (1.0 - delta)

        line = f"{param_name}={plus_value}\n"
        plus_lines = lines
        plus_lines[line_idx] = line
        with open(f"{key}_plus.input", "w") as f:
            f.writelines(plus_lines)

        line = f"{param_name}={minus_value}\n"
        minus_lines = lines
        minus_lines[line_idx] = line
        with open(f"{key}_minus.input", "w") as f:
            f.writelines(minus_lines)

        print(f"generate inputs for {param_name}")

        for i in ["plus", "minus"]:
            os.mkdir(f"{key}_{i}")
            shutil.move(f"{key}_{i}.input", f"{key}_{i}")
            shutil.copy(test_name,f"{key}_{i}")
            
    #generate input for reference
    os.mkdir("ref")
    shutil.copy(input_file,"ref/ref.input")
    shutil.copy(test_name,"ref")
    print(f"generate input for reference")
            
if  __name__ == "__main__":
    with open("sensitivity_input.dict", "r") as f:
        params = json.load(f)
    prepare_input("tr1-2.input", params)