#read in txt file as dict
import json as js
params = {"A": "ReferencePermeability",
          "B": "Exponent",
          "C": "kdiss1"}

with open("sensitivity_input.dict", "r") as f:
    t = js.load(f)

print(type(t))
