How to use sensitivity analysis
Files

sensitivity_input.dict : a json file containing the setup which parameters are tested
prepare_input.py : generates input and copy of binary file into folders
run_tests.py : runs multiple tests in parallel
sensitivity.py : analyzes the results of each case and gives a table of relevant results

Workflow:

specify parameters in sensitivity_input.dict
run python prepare_input.py in build folder. One can adjust delta for the range of parameters
run python run_tests.py, here you can define num_processors to limit the usage.
run python sensitivity.pyin build folder, with option verbal you can see the detailed output.

Improvement possibilities:

define your own way of parameters range like log etc..
define your evaluations of parameters in sensitivity.py
