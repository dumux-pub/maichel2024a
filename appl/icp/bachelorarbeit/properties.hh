// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

#ifndef DUMUX_MICP_BIOCEMENT_PROPERTIES_HH
#define DUMUX_MICP_BIOCEMENT_PROPERTIES_HH

#include <dumux/common/properties.hh>
#include <dumux/porousmediumflow/2pncmin/model.hh>
#include <dumux/porousmediumflow/2picp/model.hh>

#include <dune/grid/yaspgrid.hh>

#include <dumux/discretization/box.hh>

#include <dumux/material/fluidsystems/biocement.hh>
#include <dumux/material/fluidsystems/biocementsimplechemistry.hh>
#include <dumux/material/solidsystems/nobiofilmsolids.hh>
#include <dumux/material/binarycoefficients/brine_co2.hh>
#include <dumux/material/chemistry/biogeochemistry/biocementcarbonicacid.hh>


#include "problem.hh"
#include "spatialparams.hh"
#include <appl/icp/icpspatialparams.hh>
#include <appl/icp/co2tableslaboratory.hh>

namespace Dumux::Properties{

namespace TTag {
struct BioCementColumn { using InheritsFrom = std::tuple<TwoPICP, BoxModel>; };
struct BioCementColumnSimpleChemistry { using InheritsFrom = std::tuple<TwoPNCMin, BoxModel>; };
struct BioCementSimpleChemistry { using InheritsFrom = std::tuple<TwoPNCMin, BoxModel>; };
} // end namespace TTag

// Set the grid type
// Set the 1D grid type for the column problem
template<class TypeTag>
struct Grid<TypeTag, TTag::BioCementColumn> { using type = Dune::YaspGrid<1>; };
template<class TypeTag>
struct Grid<TypeTag, TTag::BioCementColumnSimpleChemistry> { using type = Dune::YaspGrid<1>; };
template<class TypeTag>
struct Grid<TypeTag, TTag::BioCementSimpleChemistry> { using type = Dune::YaspGrid<2>; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::BioCementColumn> { using type = BioCementColumnProblem<TypeTag>; };

//Set the CO2 tables used
template<class TypeTag>
struct CO2Tables<TypeTag, TTag::BioCementColumn> {using type = ICPCO2TablesLab::CO2Tables;};
template<class TypeTag>
struct CO2Tables<TypeTag, TTag::BioCementColumnSimpleChemistry> {using type = ICPCO2TablesLab::CO2Tables;};
template<class TypeTag>
struct CO2Tables<TypeTag, TTag::BioCementSimpleChemistry> {using type = ICPCO2TablesLab::CO2Tables;};

// set the fluidSystems used
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::BioCementColumn>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using CO2Tables = ICPCO2TablesLab::CO2Tables;
    using H2OTabulated = Components::TabulatedComponent<Components::H2O<Scalar>>;
    using type = Dumux::FluidSystems::BiocementFluid<Scalar, CO2Tables, H2OTabulated>;
};
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::BioCementColumnSimpleChemistry>
{
   using Scalar = GetPropType<TypeTag, Properties::Scalar>;
   using CO2Tables = ICPCO2TablesLab::CO2Tables;
   using H2OTabulated = Components::TabulatedComponent<Components::H2O<Scalar>>;
   using type = Dumux::FluidSystems::BiocementSimpleChemistryFluid<Scalar, CO2Tables, H2OTabulated>;
};
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::BioCementSimpleChemistry>
{
   using Scalar = GetPropType<TypeTag, Properties::Scalar>;
   using CO2Tables = ICPCO2TablesLab::CO2Tables;
   using H2OTabulated = Components::TabulatedComponent<Components::H2O<Scalar>>;
   using type = Dumux::FluidSystems::BiocementSimpleChemistryFluid<Scalar, CO2Tables, H2OTabulated>;
};

// set the solidSystem
template<class TypeTag>
struct SolidSystem<TypeTag, TTag::BioCementColumn>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = SolidSystems::NoBiofilmSolidPhase<Scalar>;
};

//Set the problem chemistries
template<class TypeTag>
struct Chemistry<TypeTag, TTag::BioCementColumn>
{
    using CO2Tables = GetPropType<TypeTag, Properties::CO2Tables>;
    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
    using type = Dumux::BioCementCarbonicAcid<TypeTag, CO2Tables, ModelTraits>;
};

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::BioCementColumn> { using type = ICPSpatialParams<TypeTag>; };
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::BioCementColumnSimpleChemistry> { using type = ICPSpatialParams<TypeTag>; };
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::BioCementSimpleChemistry> { using type = ICPSpatialParams<TypeTag>; };

template<class TypeTag>
struct Formulation<TypeTag, TTag::BioCementColumn>
{ static constexpr auto value = TwoPFormulation::p0s1; };
template<class TypeTag>
struct Formulation<TypeTag, TTag::BioCementColumnSimpleChemistry>
{ static constexpr auto value = TwoPFormulation::p0s1; };
template<class TypeTag>
struct Formulation<TypeTag, TTag::BioCementSimpleChemistry>
{ static constexpr auto value = TwoPFormulation::p0s1; };

}// We leave the namespace Dumux::Properties.
#endif
