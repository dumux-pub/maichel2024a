// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

#ifndef DUMUX_MICP_BIOCEMET_PROBLEM_HH
#define DUMUX_MICP_BIOCEMET_PROBLEM_HH

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dumux/discretization/evalgradients.hh>

#include <dumux/porousmediumflow/problem.hh>
#include <dumux/common/boundarytypes.hh> // for `BoundaryTypes`
#include <dumux/common/properties.hh> // GetPropType
#include <dumux/common/numeqvector.hh> // for `NumEqVector`
#include <dumux/material/components/ammonia.hh>
#include <dumux/material/components/sodiumion.hh>

namespace Dumux
{
/*!
 * \ingroup TwoPNCSecCompMinModel
 * \ingroup ImplicitTestProblems
 * \brief Problem for enzyme-induced calcium carbonate precipitation
 *  */
template <class TypeTag>
class BioCementColumnProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using FluxVariables = GetPropType<TypeTag, Properties::FluxVariables>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    enum {
        numComponents = FluidSystem::numComponents,

        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx, //Saturation
        xwNaIdx = FluidSystem::NaIdx,
        xwClIdx = FluidSystem::ClIdx,
        xwCaIdx = FluidSystem::CaIdx,
        xwUreaIdx = FluidSystem::UreaIdx,
        xwTNHIdx = FluidSystem::TNHIdx,
        phiCalciteIdx = numComponents,

        //Indices of the components
        wCompIdx = FluidSystem::wCompIdx,
        nCompIdx = FluidSystem::nCompIdx,
        NaIdx = FluidSystem::NaIdx,
        ClIdx = FluidSystem::ClIdx,
        CaIdx = FluidSystem::CaIdx,
        UreaIdx = FluidSystem::UreaIdx,
        TNHIdx = FluidSystem::TNHIdx,

        wPhaseIdx = FluidSystem::wPhaseIdx,

        //Index of the primary component of G and L phase
        conti0EqIdx = Indices::conti0EqIdx,

        // Phase State
        wPhaseOnly = Indices::firstPhaseOnly,
        bothPhases = Indices::bothPhases,
    };

    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using Chemistry = GetPropType<TypeTag, Properties::Chemistry>;
    static constexpr int dim = GridView::dimension;

    using NH3 = typename Components::Ammonia<Scalar>;
    using Na = typename Components::SodiumIon<Scalar>;
public:
    BioCementColumnProblem(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    {
        name_  = getParam<std::string>("Problem.Name");

        //initial values
        densityW_ = getParam<Scalar>("Initial.initDensityW");
        initPressure_ = getParam<Scalar>("Initial.initPressure");

        initxwTC_ = getParam<Scalar>("Initial.initxwTC");
        initxwNa_ = getParam<Scalar>("Initial.initxwNa");
        initxwCl_ = getParam<Scalar>("Initial.initxwCl");
        initxwCa_ = getParam<Scalar>("Initial.initxwCa");
        initxwUrea_ = getParam<Scalar>("Initial.initxwUrea");
        initxwTNH_ = getParam<Scalar>("Initial.initxwTNH");
        initCalcite_ = getParam<Scalar>("Initial.initCalcite");
        initCalciteInSand_ = getParam<Scalar>("Initial.initCalciteInSand");

        kpowdermax_= getParam<Scalar>("UreolysisCoefficients.kPowderMax");
        referencePorosity_=getParam<Scalar>("SpatialParams.ReferencePorosity");
        percentPowder_ = getParam<Scalar>("Initial.PercentPowder");
        kureasemodell_ = getParam<Scalar>("UreolysisCoefficients.KureaseModell");
        kub_ = getParam<Scalar>("UreolysisCoefficients.kub");

        initTemperature_ = getParam<Scalar>("Initial.initTemperature");

        xwNaCorr_ = getParam<Scalar>("Initial.xwNaCorr");
        xwClCorr_ = getParam<Scalar>("Initial.xwClCorr");

        //injection values
        injTC_ = getParam<Scalar>("Injection.injTC");
        injNaCorr_ = getParam<Scalar>("Injection.injNaCorr");
        injCa_ = getParam<Scalar>("Injection.injCa");
        injUrea_ = getParam<Scalar>("Injection.injUrea");
        injTNH_ = getParam<Scalar>("Injection.injTNH");

        injDensity_ = getParam<Scalar>("Injection.injDensity");

        numInjections_ = getParam<int>("Injection.numInjections");

	faktor_ = getParam<Scalar>("Neumann.faktor");
	refInjectionQ_ = getParam<Scalar>("Neumann.refInjectionQ");


        // We resize the permeability vector containing the permeabilities for the additional output
        permeability_.resize(gridGeometry->numDofs());
	porosity_.resize(gridGeometry->numDofs());
	x_.resize(gridGeometry->numDofs());



        // We read the injection rates for each episode from the injection data file.
        // We will use this in the Neumann boundary condition to set time dependend, changing boundary conditions.
        // We do this similarly to the episode ends in the main file.
        injectionQ_ = readFileToContainer<std::vector<double>>(getParam<std::string>("Injection.RateFile"));
        for(int i=0;i<injectionQ_.size();i++)
        {
            std::cout << "injectionQ_["<< i <<"] = " << injectionQ_[i] <<std::endl;
        }

//      check the injection data against the number of injections specified in the parameter file
        if (injectionQ_.size() != numInjections_)
        {
            std::cerr <<  "numInjections from the parameterfile and the number of injection types specified in the injection data file do not match!"
                    <<"\n numInjections from parameter file = "<<numInjections_
                    <<"\n injectionQ_ from injection data file = "<<injectionQ_.size()
                    <<"\n Abort!\n";
            exit(1) ;
        }

        FluidSystem::init(/*startTemp=*/initTemperature_ -5.0, /*endTemp=*/initTemperature_ +5.0, /*tempSteps=*/5,
             /*startPressure=*/1e4, /*endPressure=*/1e6, /*pressureSteps=*/500);
    }

    void setTime( Scalar time )
    {
        time_ = time;
    }

    void setTimeStepSize( Scalar timeStepSize )
    {
        timeStepSize_ = timeStepSize;
    }

    void setEpisodeIdx( Scalar epiIdx )
    {
        episodeIdx_ = epiIdx;
    }

    void setFlag( Scalar value )
    {
        flag_ = value;
    }

    Scalar getInjectionRate()
    {
    	    if(injectionQ_[episodeIdx_] < 1.e-10)
                    return 0;
            else
            return refInjectionQ_;
    }


   /*!
    * \name Problem parameters
    */


   /*!
    * \brief The problem name.
    *
    * This is used as a prefix for files generated by the simulation.
    */
//    const char *name() const
    const std::string name() const
    { return name_; }

   /*!
    * \brief Returns the temperature within the domain.
    *
    * This problem assumes a temperature of 25 degrees Celsius.
    */
    Scalar temperature() const
    {
        return initTemperature_; //
    };

   /*!
    * \name Boundary conditions
    */

    /*!
    * \brief Specifies which kind of boundary condition should be
    *        used for which equation on a given boundary segment.
    */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes bcTypes;

        Scalar zmax = this->gridGeometry().bBoxMax()[dim - 1];
        bcTypes.setAllNeumann();
        if(globalPos[dim - 1] > zmax - eps_)
            bcTypes.setAllDirichlet();

        return bcTypes;
    }

   /*!
    * \brief Evaluate the boundary conditions for a dirichlet
    *        boundary segment.
    */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        return initial_(globalPos);
    }

   /*!
    * \brief Evaluate the initial value for a control volume.
    *
    * \param globalPos The global position
    *
    * For this method, the \a values parameter stores primary
    * variables.
    */
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
        return initial_(globalPos);
    }

   /*!
    * \brief Evaluate the boundary conditions for a Neumann
    *        boundary segment.
    *
    * For this method, the \a values parameter stores the mass flux
    * in normal direction of each component. Negative values mean
    * influx.
    *
    * The units must be according to either using mole or mass fractions. (mole/(m^2*s) or kg/(m^2*s))
    */

    template<class ElementFluxVariablesCache, class SubControlVolumeFace>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFluxVariablesCache& elemFluxVarsCache,
			const SubControlVolumeFace& scvf) const
     {
         // We only have injection at the bottom, negative values for injection

         const auto& globalPos = scvf.ipGlobal();

         if (globalPos[dim - 1] > eps_)
             return NumEqVector(0.0);

         NumEqVector values(0.0);
         const Scalar diameter = 0.05;

         const Scalar pressure = elemVolVars[scvf.insideScvIdx()].pressure(0);
         const Scalar refPressure = 1.1e5;

	 if(flag_ == 0)
         {
          Scalar multiplier = 1 - (pressure - refPressure)/refPressure;
          if(injectionQ_[episodeIdx_]<1.e-10)
              multiplier = 1;
          refInjectionQ_*= multiplier;
          flag_ = 1;

         std::cout << "injection rate aktuell: " << refInjectionQ_ << std::setprecision(10);
         std::cout << "Druck aktuell: " << pressure << std::setprecision(10);
         std::cout << "multiplier aktuell: " << multiplier << std::setprecision(10);
         std::cout << std::endl;
         }
         Scalar waterFlux = refInjectionQ_/(3.14*diameter*diameter/4.); //[m/s]
         if(injectionQ_[episodeIdx_]<1.e-10)
                 waterFlux = 0;

         values[conti0EqIdx + wCompIdx] = - waterFlux * (injDensity_-injCa_-injUrea_-2 * injCa_/FluidSystem::molarMass(CaIdx)*FluidSystem::molarMass(ClIdx)) /FluidSystem::molarMass(wCompIdx);
         values[conti0EqIdx + nCompIdx] = - waterFlux * injTC_ * injDensity_ /FluidSystem::molarMass(nCompIdx);
         values[conti0EqIdx + xwCaIdx] = - waterFlux * injCa_/FluidSystem::molarMass(CaIdx);
         values[conti0EqIdx + xwUreaIdx] = - waterFlux * injUrea_ /FluidSystem::molarMass(UreaIdx);
         values[conti0EqIdx + xwNaIdx] = - waterFlux * injNaCorr_/FluidSystem::molarMass(NaIdx);
         values[conti0EqIdx + xwClIdx] = - waterFlux * injTNH_ /NH3::molarMass()                   //NH4Cl --->  mol Cl = mol NH4
                                         - waterFlux * 2 * injCa_/FluidSystem::molarMass(CaIdx)             //+CaCl2 --->  mol Cl = mol Ca*2
                                         -waterFlux *injNa_ / Na::molarMass()  ;   //    NaCl ---> mol Cl = mol Na

         return values;
     }



   /*!
    * \name Volume terms
    */
    // \{

   /*!
    * \brief Evaluate the source term for all phases within a given
    *        sub-control-volume.
    *
    * This is the method for the case where the source term is
    * potentially solution dependent and requires some quantities that
    * are specific to the fully-implicit method.
    *
    * \param values The source and sink values for the conservation equations in units of
    *                 \f$ [ \textnormal{unit of conserved quantity} / (m^3 \cdot s )] \f$
    * \param element The finite element
    * \param fvGeometry The finite-volume geometry
    * \param elemVolVars All volume variables for the element
    * \param scv The subcontrolvolume
    *
    * For this method, the \a values parameter stores the conserved quantity rate
    * generated or annihilate per volume unit. Positive values mean
    * that the conserved quantity is created, negative ones mean that it vanishes.
    * E.g. for the mass balance that would be a mass rate in \f$ [ kg / (m^3 \cdot s)] \f$.
    */
    NumEqVector source(const Element &element,
                   const FVElementGeometry& fvGeometry,
                   const ElementVolumeVariables& elemVolVars,
                   const SubControlVolume &scv) const
    {
        NumEqVector source(0.0);
        auto elemSol = elementSolution<Element, ElementVolumeVariables, FVElementGeometry>(element, elemVolVars, fvGeometry);
        const auto gradPw = evalGradients(element,
                                        element.geometry(),
                                        this->gridGeometry(),
                                        elemSol,
                                        scv.center(),
                                        true /*ignoreState*/)[pressureIdx];
        Scalar scvPotGradNorm = gradPw.two_norm();

        Chemistry chemistry;
        source = chemistry.reactionSource(elemVolVars[scv],
                        timeStepSize_,
                        scvPotGradNorm);
        return source;
    }

   /*!
    * \brief Adds additional VTK output data to the VTKWriter. Function is called by the output module on every write.
    */

    const std::vector<Scalar>& getPermeability()
    {
        return permeability_;
    }

    template<class SolutionVector>
    void updateVtkOutput(const SolutionVector& curSol)
    {
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            const auto elemSol = elementSolution(element, curSol, this->gridGeometry());

            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);

            for (auto&& scv : scvs(fvGeometry))
            {
                VolumeVariables volVars;
                volVars.update(elemSol, *this, element, scv);
                const auto dofIdxGlobal = scv.dofIndex();
                permeability_[dofIdxGlobal] = volVars.permeability();
                porosity_[dofIdxGlobal] = volVars.porosity();
                x_[dofIdxGlobal] = scv.dofPosition()[0];
            }
        }
    }

    template<class SolutionVector>
    void writeFinalPorosities(const SolutionVector& curSol)
    {
	std::ofstream outputfileporo;
	outputfileporo.open("finalporosities.dat",std::ios::out);
	outputfileporo << std::scientific << std::setprecision(4);

        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            const auto elemSol = elementSolution(element, curSol, this->gridGeometry());

            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);

            for (auto&& scv : scvs(fvGeometry))
            {
                VolumeVariables volVars;
                volVars.update(elemSol, *this, element, scv);
                const auto dofIdxGlobal = scv.dofIndex();
                porosity_[dofIdxGlobal] = volVars.porosity();
            }
        }
	for (int i=0; i<this->gridGeometry().numDofs(); i++)
	{
	  outputfileporo << x_[i] << "\t" << porosity_[i] << std::endl;
	}
	outputfileporo.close();
    }

        // from here
     //! Post processing the total masses of the system
     template<class GridVariables, class SolutionVector>
     Scalar getAveragePoro(const GridVariables& gridVars,
                                  const SolutionVector& x) const
     {
         Scalar avgPoro;
         int i = 0;

         Scalar voidCollector = 0.0;
         Scalar volCollector = 0.0;
         for( const auto& element : elements(this->gridGeometry().gridView()) )
         {
             auto fvGeometry = localView(this->gridGeometry());
             auto elemVolVars = localView(gridVars.curGridVolVars());
             fvGeometry.bind(element);
             elemVolVars.bind(element, fvGeometry, x);
             for (const auto& scv : scvs(fvGeometry))
             {
                 const auto& globalPos = scv.center();
                 if (globalPos[0] < 0.135)
                 {
                     const auto& volVars = elemVolVars[scv];
                     const Scalar porosity = volVars.porosity();
                     const Scalar vol = elemVolVars[scv].extrusionFactor()*scv.volume();
                     voidCollector += porosity*vol;
                     volCollector += vol;
                 }
             }
         }
         avgPoro = voidCollector/volCollector;
         return avgPoro; // we return for now simply a scalar value, the function could do more
     }

private:
    // internal method for the initial condition (reused for the
    // dirichlet conditions!)
    PrimaryVariables initial_(const GlobalPosition &globalPos) const
    {
        PrimaryVariables priVars(0.0);
        priVars.setState(wPhaseOnly);
        priVars[pressureIdx] = initPressure_ ; //70e5; // - (maxHeight - globalPos[1])*densityW_*9.81; //p_atm + rho*g*h
        priVars[switchIdx] = initxwTC_;
        priVars[xwNaIdx] = initxwNa_ + xwNaCorr_;
        priVars[xwClIdx] = initxwCl_ + initxwTNH_ + 2*initxwCa_ + xwClCorr_;
        priVars[xwCaIdx] = initxwCa_;
        priVars[xwUreaIdx] = initxwUrea_;
        priVars[xwTNHIdx] = initxwTNH_;
        //entfernt
        if (globalPos[0]>0.15) // checked with ILEK
        {
            priVars[phiCalciteIdx] = initCalciteInSand_ ; // [m^3/m^3]
        }
        else
        {
            priVars[phiCalciteIdx] = initCalciteInSand_ + initCalcite_; // [m^3/m^3]
        }


#if NONISOTHERMAL
        priVars[temperatureIdx] = initTemperature_;
#endif
        return priVars;
    }
    /*!
        * \brief Returns the molality of NaCl (mol NaCl / kg water) for a given mole fraction
        *
        * \param XwNaCl the XwNaCl [kg NaCl / kg solution]
        */
    static Scalar massTomoleFrac_(Scalar XwNaCl)
    {
        const Scalar Mw = FluidSystem::molarMass(wCompIdx);  // 18.015e-3; /* molecular weight of water [kg/mol] */
        const Scalar Ms = FluidSystem::molarMass(NaIdx) + FluidSystem::molarMass(ClIdx); // 58.44e-3; /* molecular weight of NaCl  [kg/mol] */

        const Scalar X_NaCl = XwNaCl;
        /* XwNaCl: conversion from mass fraction to mol fraction */
        const Scalar xwNaCl = -Mw * X_NaCl / ((Ms - Mw) * X_NaCl - Ms);
        return xwNaCl;
    }

    static constexpr Scalar eps_ = 1e-6;

    Scalar numDofs_;

    Scalar initPressure_;
    Scalar densityW_;//1087; // rhow=1087;

    Scalar initxwTC_;//2.3864e-7;       // [mol/mol]
    Scalar initxwNa_;//0;
    Scalar initxwCl_;//0;
    Scalar initxwCa_;//0;
    Scalar initxwUrea_;//0;
    Scalar initxwTNH_;//3.341641e-3;
    Scalar xwNaCorr_;//2.9466e-6;
    Scalar xwClCorr_;//0;

    Scalar initCalcite_;
    Scalar percentPowder_;
    Scalar initCalciteInSand_;
    Scalar initTemperature_;

    Scalar referencePorosity_;
    Scalar kureasemodell_;
    Scalar kpowdermax_;
    Scalar kub_;
    Scalar cureaseT_;
    Scalar dc0_;


    Scalar injDensity_;

    Scalar injTC_;             // [kg/kg]
    Scalar injNa_;             // [kg/m³]
    Scalar injCa_;             // [kg/m³]      //computed from 0.333mol/l CaCl2
    Scalar injUrea_;           // [kg/m³]
    Scalar injTNH_;            // [kg/m³]      //computed from 10 g/l NH4Cl
    Scalar injNaCorr_;

    Scalar faktor_;
    mutable Scalar refInjectionQ_;


    int numInjections_;
    std::vector<Scalar> injectionQ_;
    std::string name_;
    std::vector<Scalar> permeability_;
    std::vector<Scalar> porosity_;
    std::vector<Scalar> x_;


    Scalar time_ = 0.0;
    mutable Scalar flag_ = 1.0;
    mutable Scalar vFC_ = 0.0;
    Scalar timeStepSize_ = 0.0;
    int episodeIdx_ = 0;
};
} //end namespace

#endif

