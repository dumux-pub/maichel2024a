This file has been created automatically. Please adapt it to your needs.

## Content

The content of this DUNE module was extracted from the module `dumux-mineralization`.
In particular, the following subFolder of `dumux-mineralization` have been extracted:
*   `appl/icp/bachelorarbeit`


Additionally, all headers in `dumux-mineralization` that are required to build the
executables from the sources
*   `appl/icp/bachelorarbeit/micp_column.cc`


have been extracted. You can configure the module just like any other DUNE
module by using `dunecontrol`. For building and running the executables,
please go to the build folders corresponding to the sources listed above.


## License

This project is licensed under the terms and conditions of the GNU General Public
License (GPL) version 3 or - at your option - any later version.
The GPL can be found under [GPL-3.0-or-later.txt](LICENSES/GPL-3.0-or-later.txt)
provided in the `LICENSES` directory located at the topmost of the source code tree.


## Version Information

|      module name      |      branch name      |                 commit sha                 |         commit date         |
|-----------------------|-----------------------|--------------------------------------------|-----------------------------|
|      dune-common      |     origin/master     |  55001c9c00dda5b37fd556e99964810c7a085fd7  |  2023-04-01 13:38:20 +0000  |
|     dune-geometry     |     origin/master     |  1a4f504d95f1a5ccb46b380c316da421b770c091  |  2023-02-28 09:20:52 +0000  |
|       dune-grid       |     origin/master     |  49e6984e0d32834ca0b3af848820b6bd93924029  |  2023-04-02 11:49:11 +0000  |
|  dune-localfunctions  |     origin/master     |  8f1cb33575204e5a245e9e6088e7a6471d3903a9  |  2023-03-09 08:08:07 +0000  |
|       dune-istl       |     origin/master     |  454fd55c14133075d8a6fddc3a015599eaee0ffe  |  2023-03-27 13:54:26 +0000  |
|         dumux         |  origin/releases/3.7  |  6227ee9b3ca31ce2aa2394a834287934a6d83028  |  2023-04-26 10:20:17 +0000  |

## Installation

The installation procedure is done as follows:
Create a root folder, e.g. `DUMUX`, enter the previously created folder,
clone this repository and use the install script `install_Maichel2024a.sh`
provided in this repository to install all dependent modules.

```sh
mkdir DUMUX
cd DUMUX
git clone https://git.iws.uni-stuttgart.de/dumux-pub/maichel2024a.git Maichel2024a
./Maichel2024a/install_Maichel2024a.sh
```

This will clone all modules into the directory `DUMUX`,
configure your module with `dunecontrol` and build tests.

## Features

This module contains not only the model itself, but also the tools which were used in the associated thesis. The basefiles for the parameterestimation using PEST++-GLM are located in the folder pestfiles. The pythonskripts which were used for the local sensitivity analysis are located in appl/icp/bachelorarbeit/ with the code of the model.

## Sensitivity analysis

How to use sensitivity analysis

Files:

sensitivity_input.dict : a json file containing the setup which parameters are tested
prepare_input.py : generates input and copy of binary file into folders
run_tests.py : runs multiple tests in parallel
sensitivity.py : analyzes the results of each case and gives a table of relevant results

Workflow:

specify parameters in sensitivity_input.dict
run python prepare_input.py in build folder. One can adjust delta for the range of parameters
run python run_tests.py, here you can define num_processors to limit the usage.
run python sensitivity.pyin build folder, with option verbal you can see the detailed output.

## Parameter estimation

In the estimation version 5.2.7 of PEST++-GLM was used. The Pest-mainfile, the Pest- input and the Pest- outputfiles, which were used in the corresponding thesis, are located in /pestfiles.
