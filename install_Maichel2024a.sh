#!/bin/bash

# 
# This installs the module Maichel2024a and its dependencies.
# The exact revisions used are listed in the table below.
# However, note that this script may also apply further patches.
# If so, all patches are required to be the current folder, or,
# in the one that you specified as argument to this script.
# 
# 
# |      module name      |      branch name      |                 commit sha                 |         commit date         |
# |-----------------------|-----------------------|--------------------------------------------|-----------------------------|
# |      dune-common      |     origin/master     |  55001c9c00dda5b37fd556e99964810c7a085fd7  |  2023-04-01 13:38:20 +0000  |
# |     dune-geometry     |     origin/master     |  1a4f504d95f1a5ccb46b380c316da421b770c091  |  2023-02-28 09:20:52 +0000  |
# |       dune-grid       |     origin/master     |  49e6984e0d32834ca0b3af848820b6bd93924029  |  2023-04-02 11:49:11 +0000  |
# |  dune-localfunctions  |     origin/master     |  8f1cb33575204e5a245e9e6088e7a6471d3903a9  |  2023-03-09 08:08:07 +0000  |
# |       dune-istl       |     origin/master     |  454fd55c14133075d8a6fddc3a015599eaee0ffe  |  2023-03-27 13:54:26 +0000  |
# |         dumux         |  origin/releases/3.7  |  6227ee9b3ca31ce2aa2394a834287934a6d83028  |  2023-04-26 10:20:17 +0000  |

exitWithError()
{
    MSG=$1
    echo "$MSG"
    exit 1
}

installModule()
{
    FOLDER=$1
    URL=$2
    BRANCH=$3
    REVISION=$4

    if [ ! -d "$FOLDER" ]; then
        if ! git clone $URL; then exitWithError "clone failed"; fi
        pushd $FOLDER
            if ! git checkout $BRANCH; then exitWithError "checkout failed"; fi
            if ! git reset --hard $REVISION; then exitWithError "reset failed"; fi
        popd
    else
        echo "Skip cloning $URL since target folder "$FOLDER" already exists."
    fi
}

applyPatch()
{
    FOLDER=$1
    PATCH=$2

    pushd $FOLDER
        echo "$PATCH" > tmp.patch
        if ! git apply tmp.patch; then exitWithError "patch failed"; fi
        rm tmp.patch
    popd
}

TOP="."
mkdir -p $TOP
cd $TOP

echo "Installing dune-common"
installModule dune-common https://gitlab.dune-project.org/core/dune-common.git origin/master 55001c9c00dda5b37fd556e99964810c7a085fd7
echo "Installing dune-geometry"
installModule dune-geometry https://gitlab.dune-project.org/core/dune-geometry.git origin/master 1a4f504d95f1a5ccb46b380c316da421b770c091
echo "Installing dune-grid"
installModule dune-grid https://gitlab.dune-project.org/core/dune-grid.git origin/master 49e6984e0d32834ca0b3af848820b6bd93924029
echo "Installing dune-localfunctions"
installModule dune-localfunctions https://gitlab.dune-project.org/core/dune-localfunctions.git origin/master 8f1cb33575204e5a245e9e6088e7a6471d3903a9
echo "Installing dune-istl"
installModule dune-istl https://gitlab.dune-project.org/core/dune-istl.git origin/master 454fd55c14133075d8a6fddc3a015599eaee0ffe
echo "Installing dumux"
installModule dumux https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git origin/releases/3.7 6227ee9b3ca31ce2aa2394a834287934a6d83028
echo "Configuring project"
if ! ./dune-common/bin/dunecontrol --opts=Maichel2024a/cmake.opts all; then
    echo "Configuration of the project failed"
    exit 1
fi
